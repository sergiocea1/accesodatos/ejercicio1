<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Ciclista;
use app\models\Lleva;
use app\models\Puerto;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;



class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    
    //Consulta 1
    public function actionConsultadao(){
        $numero = Yii::$app->db
                ->createCommand('select distinct edad from ciclista')
                ->queryScalar();
        
        $dataProvider = new SqldataProvider([
            'sql' => 'SELECT DISTINCT edad FROM ciclista',
            'totalCount' => $numero,
            'pagination' =>[
                'pageSize' => 5,
                
            ]
        ]);
        
        return $this->render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetidos)",
            "sql"=>"SELECT DISTINCT edad FROM ciclista",
        ]);
    }
    
    public function actionConsultaorm(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("edad")->distinct(),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetidos)",
            "sql"=>"SELECT DISTINCT edad FROM ciclista",
        ]);
    }
    //Consulta 2
    public function actionConsultadao2(){
        $numero = Yii::$app->db
                ->createCommand('select count(distinct edad) from ciclista where nomequipo="Artiach"')
                ->queryScalar();
        
        $dataProvider = new SqldataProvider([
            'sql' => 'SELECT DISTINCT edad FROM ciclista WHERE nomequipo = "Artiach"',
            'totalCount' => $numero,
            'pagination' =>[
                'pageSize' => 5,
                
            ]
        ]);
        
        return $this->render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach'",
        ]);
    }
    
    public function actionConsultaorm2(){
        
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("edad")->distinct()->where("nomequipo = 'Artiach'"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 2 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach'",
        ]);
    }
    //Consulta 3
    public function actionConsultadao3(){
        $numero = Yii::$app->db
                ->createCommand('select count(distinct edad) from ciclista where nomequipo ="Artiach" or nomequipo = "Amore Vita"')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT DISTINCT edad FROM ciclista WHERE nomequipo = "Artiach" OR nomequipo = "Amore Vita"',
            'totalCount' => $numero,
            'pagination' =>[
                'pageSize'=>5
            ] 
        ]);
        
        return $this -> render("resultados",[
           "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach o Amore Vita",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach' OR nomequipo = 'Amore Vita'",
        ]);
    }
    public function actionConsultaorm3(){
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("edad")->distinct()->where("nomequipo = 'Artiach'")->orWhere("nomequipo = 'Amore Vita'"),
            'pagination' =>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach o Amore Vita",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach' OR nomequipo = 'Amore Vita'",
        ]);
    }
    //Consulta 4
    public function actionConsultadao4(){
       
        $numero = Yii::$app->db
                ->createCommand('select count(dorsal) from ciclista where edad < 25 or edad >30')
                ->queryScalar();
        
        
        $dataProvider = new SqldataProvider([
            'sql' => 'SELECT dorsal FROM ciclista WHERE edad < 25 OR edad > 30',
            'totalCount' => $numero,
            'pagination' => [
                'pageSize' => 5,
            ]
        ]);
        
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=> ['dorsal'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql"=>"SELECT dorsal FROM ciclista WHERE edad < 25 OR edad > 30"
        ]);
    }
    public function actionConsultaorm4(){
        $dataProvider = new ActiveDataProvider([
            'query'=>Ciclista::find()->select('dorsal')->where('edad < 25')->orWhere('edad >30'),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 4 con Active Record",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql"=>"SELECT dorsal FROM ciclista WHERE edad < 25 OR edad > 30"
        ]);
    }
    //Consulta 5
    public function actionConsultadao5(){
        $numero = Yii::$app->db
                ->createCommand('select count(dorsal) from ciclista where nomequipo="Banesto" and edad between 28 and 32')
                ->queryScalar();
        
        $dataProvider = new SqldataProvider([
            'sql' => 'SELECT dorsal FROM ciclista WHERE nomequipo="Banesto" AND edad BETWEEN 28 AND 32',
            'totalCount' => $numero,
            'pagination' =>[
                'pageSize'=>5,
            ]
        ]);
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 5 de DAO",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
            "sql"=>"SELECT dorsal FROM ciclista WHERE nomequipo='Banesto' AND edad BETWEEN 28 AND 32"
        ]);
    }
    public function actionConsultaorm5(){
        $dataProvider = new ActiveDataProvider([
            'query' =>Ciclista::find()->select('dorsal')->where("nomequipo = 'Banesto'")->andWhere("edad between 28 and 32"),
            'pagination'=>[
                'pageSize'=>5
            ]
        ]);
        
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 5 de Active Record",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
            "sql"=>"SELECT dorsal FROM ciclista WHERE nomequipo='Banesto' AND edad BETWEEN 28 AND 32"
        ]);
    }
    //Consulta 6
    public function actionConsultadao6(){
        $numero = Yii::$app->db
                ->createCommand('select count(nombre) from ciclista where length(nombre)>8')
                ->queryScalar();
        
        $dataProvider = new SqldataProvider([
            'sql'=>'SELECT nombre FROM ciclista WHERE length(nombre)>8',
            'totalCount'=>$numero,
            'pagination'=>[
                'pagesize'=>5,
            ]
        ]);
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 6 DAO",
            "enunciado"=>"Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
            "sql"=>"SELECT nombre FROM ciclista WHERE length(nombre)>8"
        ]);
    }
    public function actionConsultaorm6(){
        $dataProvider = new ActiveDataProvider([
            'query'=>Ciclista::find()->select('nombre')->where("length(nombre)>8"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 6 con Active Record",
            "enunciado"=>"Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
            "sql"=>"SELECT nombre FROM ciclista WHERE length(nombre)>8"
        ]);
    }
    //Consulta 7
    public function actionConsultadao7(){
        $numero = Yii::$app->db
                ->createCommand('select count(nombre) from ciclista')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre, dorsal, upper(nombre) AS nombre_mayusculas FROM ciclista',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','dorsal','nombre_mayusculas'],
            "titulo"=>"Consulta 7 con DAO",
            "enunciado"=>"Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
            "sql"=>"SELECT nombre, dorsal, upper(nombre) AS nombre_mayusculas FROM ciclista"
        ]);
                  
        }
    public function actionConsultaorm7(){
        
        $dataProvider = new ActiveDataProvider([
            'query'=>Ciclista::find()->select(['nombre','dorsal','upper(nombre) AS nmayus']),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','dorsal','nmayus'],
            "titulo"=>"Consulta 7 con Active Record",
            "enunciado"=>"Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
            "sql"=>"SELECT nombre, dorsal, upper(nombre) AS nmayus FROM ciclista"
        ]);
    }
    //Consulta 8
    public function actionConsultadao8(){
        $numero = Yii::$app->db
                ->createCommand('select count(distinct dorsal) from lleva where código="MGE"')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT dorsal FROM lleva WHERE código="MGE"',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>"Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
            "sql"=>"SELECT DISTINCT dorsal FROM lleva WHERE maillot='MGE'"
        ]);
                  
        }
    public function actionConsultaorm8(){
        $dataProvider = new ActiveDataProvider([
            'query'=>Lleva::find()->select('dorsal')->distinct()->where("código='MGE'"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 8 con Active Record",
            "enunciado"=>"Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
            "sql"=>"SELECT DISTINCT dorsal FROM lleva WHERE código='MGE'"
        ]);
    }
    //Consulta 9
    public function actionConsultadao9(){
        $numero = Yii::$app->db
                ->createCommand('select count(nompuerto) from puerto where altura > 1500')
                ->queryScalar();
        
        $dataProvider = new SqldataProvider([
            'sql'=>'SELECT nompuerto FROM puerto WHERE altura > 1500',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>5,
            ] 
        ]);
        
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 9 con DAO",
            "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500",
            "sql"=>"SELECT nompuerto FROM puerto WHERE altura > 1500"
        ]);
    }
    public function actionConsultaorm9(){
        $dataProvider = new ActiveDataProvider([
            'query'=>Puerto::find()->select('nompuerto')->distinct()->where("altura > 1500"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 9 con Active Record",
            "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500",
            "sql"=>"SELECT nompuerto FROM puerto WHERE altura > 1500"
        ]);
    }
    //Consulta 10
    public function actionConsultadao10(){
        $numero = Yii::$app->db
                ->createCommand('select count(distinct dorsal) from puerto where pendiente > 8 OR altura between 1800 AND 3000')
                ->queryScalar();
                
        $dataProvider = new SqldataProvider([
            'sql'=>'SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 OR altura BETWEEN 1800 AND 3000',
            'totalCount'=> $numero,
            'pagination'=>[
                'pageSize'=>5,
            ]    
        ]);
        
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 10 con DAO",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 OR altura BETWEEN 1800 AND 3000"
        ]);
    }
    public function actionConsultaorm10(){
        $dataProvider = new ActiveDataProvider([
            'query'=>Puerto::find()->select('dorsal')->distinct()->where("pendiente > 8")->orWhere("altura between 1800 and 3000"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 10 con Active Record",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 OR altura BETWEEN 1800 AND 3000"
        ]);
        
    }
    
    public function actionConsultadao11(){
        $numero = Yii::$app->db
                ->createCommand('select count(distinct dorsal) from puerto where pendiente > 8 AND altura between 1800 AND 3000')
                ->queryScalar();
                
        $dataProvider = new SqldataProvider([
            'sql'=>'SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 AND altura BETWEEN 1800 AND 3000',
            'totalCount'=> $numero,
            'pagination'=>[
                'pageSize'=>5,
            ]    
        ]);
        
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 10 con DAO",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 AND altura BETWEEN 1800 AND 3000"
        ]);
    }
    public function actionConsultaorm11(){
        $dataProvider = new ActiveDataProvider([
            'query'=>Puerto::find()->select('dorsal')->distinct()->where("pendiente > 8")->andWhere("altura between 1800 and 3000"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        return $this -> render("resultados",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 11 con Active Record",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 AND altura BETWEEN 1800 AND 3000"
        ]);
        
    }


}
