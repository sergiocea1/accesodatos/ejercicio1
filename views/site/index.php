<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Consultas de Selección';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Consultas de Selección</h1>
        <h2>Ejercicio 1</h2>

    <div class="body-content">
        <div class="col-sm-6 col-sm-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1</h3>
                        <p>Listar las edades de los ciclistas (sin repetidos)</p>
                        <div>
                            <?= Html::a('Active Record', ['site/consultaorm'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consultadao'], ['class' => 'btn btn-default'])?>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-sm-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 2</h3>
                        <p>Listar las edades de los ciclistas de Artiach</p>
                        <div>
                            <?= Html::a('Active Record', ['site/consultaorm2'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consultadao2'], ['class' => 'btn btn-default'])?>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-sm-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 3</h3>
                        <p>Listar las edades de los ciclistas de Artiach o de Amore Vita</p>
                        <div>
                            <?= Html::a('Active Record', ['site/consultaorm3'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consultadao3'], ['class' => 'btn btn-default'])?>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-sm-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 4</h3>
                        <p>Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30</p>
                        <div>
                            <?= Html::a('Active Record', ['site/consultaorm4'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consultadao4'], ['class' => 'btn btn-default'])?>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-sm-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 5</h3>
                        <p>Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto</p>
                        <div>
                            <?= Html::a('Active Record', ['site/consultaorm5'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consultadao5'], ['class' => 'btn btn-default'])?>
                        </div>
                    </div>
                </div>
            </div>
           
            </div>
            
            <div class="col-sm-6 col-sm-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 6</h3>
                        <p>Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8</p>
                        <div>
                            <?= Html::a('Active Record', ['site/consultaorm6'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consultadao6'], ['class' => 'btn btn-default'])?>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-sm-4">
                <div class="thumbnail">
                    <div class="caption overflow-auto">
                        <h3>Consulta 7</h3>
                        <p  >Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas</p>
                    </div>    
                        <div>
                            <?= Html::a('Active Record', ['site/consultaorm7'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consultadao7'], ['class' => 'btn btn-default'])?>
                        </div>
                    
                </div>
            </div>
           
            <div class="col-sm-6 col-sm-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 8</h3>
                        <p>Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa</p>
                        <div>
                            <?= Html::a('Active Record', ['site/consultaorm8'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consultadao8'], ['class' => 'btn btn-default'])?>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-sm-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 9</h3>
                        <p>Listar el nombre de los puertos cuya altura sea mayor de 1500 </p>
                        <div>
                            <?= Html::a('Active Record', ['site/consultaorm9'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consultadao9'], ['class' => 'btn btn-default'])?>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-sm-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 10</h3>
                        <p>Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000</p>
                        <div>
                            <?= Html::a('Active Record', ['site/consultaorm10'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consultadao10'], ['class' => 'btn btn-default'])?>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-sm-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 11</h3>
                        <p>Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000</p>
                        <div>
                            <?= Html::a('Active Record', ['site/consultaorm11'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consultadao11'], ['class' => 'btn btn-default'])?>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
